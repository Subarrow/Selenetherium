Selenetherium
=====
STOMP is a simple protocol, and this simplicity is a double-edged sword – there's a lot of libraries available which were created for a specific use case, but didn't fully support the protocol, and then weren't maintained.

Selenetherium is a simple STOMP client tester, taking the form of a STOMP server, based on Platybelodon. The object is to quickly test whether a client library supports the STOMP functionality necessary to use National Rail Open Data feeds. This is not a comprehensive tester.

The client
-----
You'll have to write a short program using the library you want to test. The program must:
1. Connect to the server (localhost:12345 via ipv4 by default), setting heartbeats to 1000,1000 if the client supports heartbeats. The username and password are optional, and can be set to anything.
2. Subscribe to test_queue, with acknowledgements set to client
3. Acknowledge all messages received

Once the client disconnects, the server will log a report, noting which features the client
used, and which versions those features are compatible with. If a version appears in every
list in the report, it's one the client supports, but if a list associated with any report
entry is empty, the behaviour isn't supported in any version (e.g. if a client fails the
content-length test).

Various examples are included in the examples directory.

Build
-----
    $ rebar3 compile

Run
-----
    If you're not making a release, you can simply run

    $ rebar3 shell