package main

import (
	"github.com/go-stomp/stomp/v3"
)

var serverAddr = "localhost:12345"
var queueName = "test_queue"
var stop = make(chan bool)

var options []func(*stomp.Conn) error = []func(*stomp.Conn) error{
	stomp.ConnOpt.Login("username", "password"),
	stomp.ConnOpt.Host("/"),
}

func main() {
	subscribed := make(chan bool)
	go recvMessages(subscribed)
	<-subscribed

	<-stop
}

func recvMessages(subscribed chan bool) {
	defer func() {
		stop <- true
	}()

	conn, err := stomp.Dial("tcp", serverAddr, options...)

	if err != nil {
		println("Connect error: ", err.Error())
		return
	}

	sub, err := conn.Subscribe(queueName, stomp.AckClient)
	if err != nil {
		println("Error on subscribe: ", queueName, err.Error())
		return
	}
	close(subscribed)

	for i := 1; i <= 10; i++ {
		msg := <-sub.C
        conn.Ack(msg)
		println(string(msg.Body))
	}
}
