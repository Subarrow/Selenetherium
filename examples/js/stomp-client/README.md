# About the library
https://www.npmjs.com/package/stomp-client

Nominally supports 1.0, 1.1. 

# Report
```
                 connect_command [10,11,12]       Command is CONNECT, supported in all STOMP versions
           accept_version_header [10]             accept-version header is undefined, only allowed in STOMP 1.0
                     host_header [10]             host header is undefined, this is only allowed in 1.0
```

I tested version 0.9.0, with the version string set to 1.1. The `session` header is ambiguously defined in 1.0, but is optional in STOMP 1.1.

```
node:events:492
      throw er; // Unhandled 'error' event
      ^

Error: Header "session" is required for CONNECTED
```

With 1.0, if you're willing to make the assumption that the session header is mandatory:
```
                 connect_command [10,11,12]       Command is CONNECT, supported in all STOMP versions
           accept_version_header [10]             accept-version header is undefined, only allowed in STOMP 1.0
                     host_header [10]             host header is undefined, this is only allowed in 1.0
               subscribe_command [10,11,12]       Specified in all versions
             subscribe_id_header [10]             SUBSCRIBE id header unset, this is only permitted in 1.0
            subscribe_ack_header [10,11,12]       SUBSCRIBE ack header set to client, supported in all versions
                     ack_command [10,11,12]       Specified in all versions
                     ack_command [10,11,12]       Specified in all versions
```

# Conclusion
Support for 1.0 only, with the caveat that it assumes an ambiguously defined header ("session" in a CONNECTED frame) is mandatory.
Supports acknowledgements, content-length.

Nominal 1.1 support is negotiated incorrectly, also assumes that header is mandatory (in 1.1 this was clarified as optional),
and will result in the server thinking it's talking to a 1.0 client.

Last release was on 2017-02-27.
