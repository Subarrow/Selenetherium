var StompClient = require('stomp-client').StompClient;

var client = new StompClient('localhost', 12345, 'testuser', 'testpass', '1.0');

client.connect(function(sessionId) {
    client.subscribe('test_queue', {'ack': 'client'}, function(body, headers) {
        client.ack(headers['message-id']);
        console.log(body);
    });
});
