# About the library

# Report
```
                 connect_command [10,11,12]       Command is CONNECT, supported in all STOMP versions
           accept_version_header [10,11,12]       Known versions from accept-version header, which is: 1.0,1.1,1.2
                     host_header [10,11,12]       host header is set, as required in 1.1 and 1.2 (meaningless but permitted in 1.0)
               subscribe_command [10,11,12]       Specified in all versions
             subscribe_id_header [10,11,12]       SUBSCRIBE id header set, as required from 1.1 and onwards, but optional in 1.0
            subscribe_ack_header [10,11,12]       SUBSCRIBE ack header set to client, supported in all versions
                     ack_command [10,11,12]       Specified in all versions
                     ack_command [10,11,12]       Specified in all versions
```

Version 1.0.0

# Conclusion
Support for 1.2, acknowledgements, content-length. Heartbeat support not tested, but is referenced and is probably functional.