const stompit = require('stompit');

const connectOptions = {
  'host': 'localhost',
  'port': 12345,
  'connectHeaders':{
    'host': '/',
    'login': 'username',
    'passcode': 'password'
  }
};

stompit.connect(connectOptions, function(error, client) {
  
  if (error) {
    console.log('connect error ' + error.message);
    return;
  }
  
  const subscribeHeaders = {
    'destination': 'test_queue',
    'ack': 'client'
  };
  
  client.subscribe(subscribeHeaders, function(error, message) {
    
    if (error) {
      console.log('subscribe error ' + error.message);
      return;
    }
    
    message.readString('utf-8', function(error, body) {
      
      if (error) {
        console.log('read message error ' + error.message);
        return;
      }
      
      console.log('received: ' + body);
      
      client.ack(message);
    });
  });
});
