# About the library
https://github.com/stomp-php/stomp-php
https://packagist.org/packages/stomp-php/stomp-php

# Report

When the optional `session` and `server` headers aren't supplied in a 1.2 CONNECTED frame:

```
                 connect_command [10,11,12]       Command is CONNECT, supported in all STOMP versions
           accept_version_header [10,11,12]       Known versions from accept-version header, which is: 1.0,1.1,1.2
                     host_header [10,11,12]       host header is set, as required in 1.1 and 1.2 (meaningless but permitted in 1.0)
                 connect_command [10,11,12]       Command is CONNECT, supported in all STOMP versions
           accept_version_header [10,11,12]       Known versions from accept-version header, which is: 1.0,1.1,1.2
                     host_header [10,11,12]       host header is set, as required in 1.1 and 1.2 (meaningless but permitted in 1.0)
               subscribe_command [10,11,12]       Specified in all versions
             subscribe_id_header [10,11,12]       SUBSCRIBE id header set, as required from 1.1 and onwards, but optional in 1.0
            subscribe_ack_header [10,11,12]       SUBSCRIBE ack header set to client, supported in all versions
                 connect_command [10,11,12]       Command is CONNECT, supported in all STOMP versions
           accept_version_header [10,11,12]       Known versions from accept-version header, which is: 1.0,1.1,1.2
                     host_header [10,11,12]       host header is set, as required in 1.1 and 1.2 (meaningless but permitted in 1.0)
```

And when it is:
```
                 connect_command [10,11,12]       Command is CONNECT, supported in all STOMP versions
           accept_version_header [10,11,12]       Known versions from accept-version header, which is: 1.0,1.1,1.2
                     host_header [10,11,12]       host header is set, as required in 1.1 and 1.2 (meaningless but permitted in 1.0)
               subscribe_command [10,11,12]       Specified in all versions
             subscribe_id_header [10,11,12]       SUBSCRIBE id header set, as required from 1.1 and onwards, but optional in 1.0
            subscribe_ack_header [10,11,12]       SUBSCRIBE ack header set to client, supported in all versions
                     ack_command [10,11,12]       Specified in all versions
                     ack_command [10,11,12]       Specified in all versions
```

Version tested: 5.1.0

# Conclusion

Sends multiple CONNECT frames, eventually dies with `Unexpected response received. Expected a "CONNECTED" Frame to determine Version. Got a "Message" Frame`. If the optional `session` and `server` headers are supplied in the CONNECTED frame, this behaviour isn't exhibited.

Library appears to be maintained, so I made a bugreport.

Other than this bug, supports 1.2, supports acknowledgements, content-length, documentation references heartbeating.