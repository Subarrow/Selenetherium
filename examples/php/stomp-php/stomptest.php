<?php

require('vendor/autoload.php');

use Stomp\Client;
use Stomp\StatefulStomp;
use Stomp\Transport\Message;

$client = new Client('tcp://localhost:12345');

$stomp = new StatefulStomp($client);
$stomp->subscribe('test_queue', 'test_queue_placeholder', 'client');

while(1) {
    $message = $stomp->read();
    echo $message;
    $stomp->ack($message);
    usleep(20000);
}

?>