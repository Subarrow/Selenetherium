# About the library

https://pecl.php.net/package/stomp
https://www.php.net/manual/en/class.stomp.php
https://pecl.php.net/package-changelog.php?package=stomp

"Official" PHP STOMP library, version 2.0.0 and onwards (2016-05-26) appear to just fix
compatibility issues with newer versions of PHP, rather than changing the underlying
functionality.

# Report

Tested version:  2.0.3 (released 2022-05-31)

```
                 connect_command [10,11,12]       Command is CONNECT, supported in all STOMP versions
           accept_version_header [10]             accept-version header is undefined, only allowed in STOMP 1.0
                     host_header [10]             host header is undefined, this is only allowed in 1.0
               subscribe_command [10,11,12]       Specified in all versions
             subscribe_id_header [10]             SUBSCRIBE id header unset, this is only permitted in 1.0
            subscribe_ack_header [10,11,12]       SUBSCRIBE ack header set to client, supported in all versions
                     ack_command [10,11,12]       Specified in all versions
                     ack_command [10,11,12]       Specified in all versions
```

Conclusion: STOMP 1.0 only, supports acknowledgements, content-length, no unilateral heartbeating