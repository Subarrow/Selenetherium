<?php

$stomp = new Stomp('tcp://localhost:12345', 'testuser', 'testpass', array());

$subscribed = $stomp->subscribe('test_queue');

while($subscribed){
    if ($stomp->hasFrame()) {
        $frame = $stomp->readFrame();
        if ($frame != NULL) {
            print($frame->body . "\n");
            $stomp->ack($frame);
        }
    }
}
?>