#!/usr/bin/perl
 
use strict;
use warnings;
use Net::STOMP::Client;
  
our(%Option, $Conn, $Start);
  
$Option{user} = "username";
$Option{password} = "password";
$Option{host} = "localhost";
$Option{port} = 12345;
$Option{destination} = "test_queue";
  
sub callback ($$) {
    my($self, $frame) = @_;
    $self->ack(frame => $frame);
    printf("received: %s\n", $frame->body());
    return($self);
}
  
$Conn = Net::STOMP::Client->new(
    host => $Option{host},
    port => $Option{port},
);
$Conn->connect(
    login    => $Option{user},
    passcode => $Option{password},
);
printf("connected to %s:%d STOMP version %s\n",
       $Conn->peer()->addr(), $Conn->peer()->port(), $Conn->version());
  
$Conn->message_callback(\&callback);
$Conn->subscribe(
    destination => $Option{destination},
    id          => 0,
    ack         => "client",
);
  
$Conn->wait_for_frames() while 1;
