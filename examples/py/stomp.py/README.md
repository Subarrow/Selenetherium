# About the library
https://pypi.org/project/stomp.py/

# Report
```
                 connect_command [11,12]          STOMP command for connect frame was first specified in 1.1
           accept_version_header [12]             Known versions from accept-version header, which is: 1.2
                     host_header [10,11,12]       host header is set, as required in 1.1 and 1.2 (meaningless but permitted in 1.0)
               subscribe_command [10,11,12]       Specified in all versions
             subscribe_id_header [10,11,12]       SUBSCRIBE id header set, as required from 1.1 and onwards, but optional in 1.0
            subscribe_ack_header [10,11,12]       SUBSCRIBE ack header set to client, supported in all versions
                     ack_command [10,11,12]       Specified in all versions
                     ack_command [10,11,12]       Specified in all versions
```

Tested version: 8.1.0

# Conclusion

Supports 1.2, acknowledgements, content-length, heartbeating, etc