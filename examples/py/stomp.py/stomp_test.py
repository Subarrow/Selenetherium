#!/usr/bin/env python3

from stomp import Connection12, ConnectionListener

class StompTest(ConnectionListener):
    def __init__(self, conn):
        self.conn = conn

    def on_error(self, frame):
        pass

    def on_message(self, frame):
        print(f'received a message: {frame.body}')
        self.conn.ack(frame.headers["ack"], 1)

    def on_disconnected(self):
        print('disconnected')

if __name__ == "__main__":
    c = Connection12([('127.0.0.1', 12345)])
    c.connect('username', 'password', wait=True)
    c.set_listener('', StompTest(c))
    c.subscribe('test_queue', id=1, ack='client')
    while True:
        pass
