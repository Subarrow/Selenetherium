# About the library
* https://github.com/stompgem/stomp
* https://rubygems.org/gems/stomp

# Report
```
                 connect_command [10,11,12]       Command is CONNECT, supported in all STOMP versions
           accept_version_header [11]             Known versions from accept-version header, which is: 1.1
                     host_header [10,11,12]       host header is set, as required in 1.1 and 1.2 (meaningless but permitted in 1.0)
               subscribe_command [10,11,12]       Specified in all versions
             subscribe_id_header [10,11,12]       SUBSCRIBE id header set, as required from 1.1 and onwards, but optional in 1.0
            subscribe_ack_header [10,11,12]       SUBSCRIBE ack header set to client, supported in all versions
                     ack_command [10,11,12]       Specified in all versions
                     ack_command [10,11,12]       Specified in all versions
```

Tested version: 1.4.10

# Conclusion
Highest supported version is 1.1, supports content-length and acknowledgements.