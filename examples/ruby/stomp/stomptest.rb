require "stomp"

class StompTest
  def initialize
    @hostname = 'localhost'
    @username = 'username'
    @password = 'password'
  end

  def run
    client_headers = { "accept-version" => "1.1", "host" => @hostname }
    client_hash = { :hosts => [ { :login => @username, :passcode => @password, :host => @hostname, :port => 12345 } ], :connect_headers => client_headers }

    client = Stomp::Client.new(client_hash)

    raise "Connection failed" unless client.open?
    raise "Connect error: #{client.connection_frame().body}" if client.connection_frame().command == Stomp::CMD_ERROR
    raise "Unexpected protocol level #{client.protocol}" unless client.protocol == Stomp::SPL_11

    puts "Connected to #{client.connection_frame().headers['server']} server with STOMP #{client.connection_frame().headers['version']}"

    client.subscribe("test_queue", { 'id' => client.uuid(), 'ack' => 'client' }) do |msg|
      puts msg.body
      client.acknowledge(msg, msg.headers)
    end

    client.join
    client.close
    puts "Client close complete"
  end
end

e = StompTest.new
e.run
