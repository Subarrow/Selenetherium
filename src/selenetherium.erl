-module(selenetherium).

-include("_build/default/lib/platybelodon/src/stomp_frame.hrl").

-export([server_link/0, server/0, client_proc/1]).

server_link() ->
    {ok, spawn_link(?MODULE, server, [])}.

server() ->
    case gen_tcp:listen(12345, [binary, {active, false}, {ip, any}, inet, {reuseaddr, true}]) of
        {ok, SSock} ->
            accept_loop(SSock);
        Other ->
            logger:error("Listen returned ~w, quitting", [Other])
        end.

accept_loop(SSock) ->
    case gen_tcp:accept(SSock) of
        {ok, CSock} ->

            {ok, Peername} = inet:peername(CSock),
            logger:notice("Accepted connection from ~w", [Peername]),
            spawn(?MODULE, client_proc, [CSock]),
            accept_loop(SSock);
        Other ->
            logger:error("Accept returned ~w, quitting accept loop", [Other])
    end.

client_proc(CSock) ->
    {ok, PeerName} = inet:peername(CSock),
    Determinations = client_loop(#{csock => CSock, peer_name => PeerName}, {command, undefined, undefined, <<"">>}, []),
    logger:notice("Final report on client from ~w:\n~s", [PeerName, format_determinations(Determinations)]).

client_loop(State = #{csock := CSock, peer_name := PeerName}, {Command, Frame, Length, Remainder}, Determinations) ->
    case gen_tcp:recv(CSock, 0) of
        {ok, NewData} ->
            {ParseState, OldFrames} = platybelodon_frame:parse_all({Command, Frame, Length, <<Remainder/binary, NewData/binary>>}),
                        
            Frames = [SingleFrame#stomp_frame{headers = platybelodon_frame:headers_to_map(Headers)} || SingleFrame = #stomp_frame{headers=Headers} <- OldFrames],

            [receipt(SingleFrame, CSock) || SingleFrame <- Frames],

            NewDeterminations = lists:flatten([determination_from_frame(SingleFrame) || SingleFrame <- Frames]),

            NewState = react_to_frames(Frames, State),
            
            client_loop(NewState, ParseState, Determinations ++ NewDeterminations);
        {error, Reason} ->
            logger:notice("Connection from ~w dropped for reason ~w", [PeerName, Reason]),
            Determinations
        end.

format_determinations(Determinations) ->
    [io_lib:format("~32s ~-16w ~s\n", [CheckName, Versions, Remark]) || {CheckName, Versions, Remark} <- Determinations].

send(CSock, Frame) ->
    gen_tcp:send(
        CSock,
        platybelodon_frame:format(Frame)
    ).

receipt(#stomp_frame{command = Command, headers = #{<<"receipt">> := ReceiptId} }, CSock) when Command =/= connect ->
    send(CSock, #stomp_frame{
        command=receipt,
        headers=[
            {<<"receipt-id">>, ReceiptId}
        ]
    });

receipt(#stomp_frame{}, _CSock) ->
    ok.

react_to_frames([#stomp_frame{command = Command, headers = Headers} | Frames], State = #{csock := CSock}) when Command =:= connect; Command =:= stomp ->
    {_, Versions, _} = accept_version(Headers),
    MaxVersion = lists:max(Versions),

    ConnectedHeaders = case MaxVersion of
        % 1.0 defines the session header ambiguously, it's not clear whether it's mandatory or not
        10 -> [{<<"session">>, <<"placeholder">>}];
        % in later versions, the version header is mandatory, but session is clearly not.
        _ -> [{<<"version">>, version_int_to_bin(MaxVersion)}]
    end,

    send(CSock, #stomp_frame{
        command=connected,
        headers=ConnectedHeaders
    }
    ),
    react_to_frames(Frames, maps:put(version, MaxVersion, State));

react_to_frames([#stomp_frame{command = subscribe, headers = Headers} | Frames], State = #{csock := CSock}) ->

    IdHeader = maps:get(<<"id">>, Headers, <<"undefined">>),
    DestinationHeader = maps:get(<<"destination">>, Headers, <<"undefined">>),

    send(CSock, #stomp_frame{
        command=message,
        headers=[
            {<<"subscription">>, IdHeader},
            {<<"destination">>, DestinationHeader},
            {<<"message-id">>, <<"message_id_1">>},
            {<<"ack">>, <<"ack_1">>}
        ],
        body = <<"hello">>
    }),

    send(CSock, #stomp_frame{
        command=message,
        headers=[
            {<<"subscription">>, IdHeader},
            {<<"destination">>, DestinationHeader},
            {<<"message-id">>, <<"message_id_2">>},
            {<<"ack">>, <<"ack_2">>}
        ],
        body = <<"Injection part I", 0, "\n",
            "MESSAGE\n",
            "subscription:", IdHeader/binary, "\n",
            "destination:", DestinationHeader/binary, "\n",
            "message-id:", "content-length-fail", "\n",
            "ack:", "content-length-fail", "\n",
            "\nInjection Part II">>
    }),

    react_to_frames(Frames, State);

react_to_frames([_Frame | Frames], State) -> react_to_frames(Frames, State);

react_to_frames([], State) -> State.

determination_from_frame(#stomp_frame{command = Command, headers = Headers}) when Command =:= connect; Command =:= stomp ->
    ConnectCommand = case Command of
        connect ->
            {connect_command, [1_0, 1_1, 1_2], <<"Command is CONNECT, supported in all STOMP versions">>};
        stomp ->
            {connect_command, [1_1, 1_2], <<"STOMP command for connect frame was first specified in 1.1">>}
        end,
    Host = host(Headers),
    AcceptVersion = accept_version(Headers),
    [ConnectCommand, AcceptVersion, Host];

determination_from_frame(#stomp_frame{command = disconnect}) ->
    {disconnect_command, [1_0, 1_1, 1_2], <<"Specified in all versions">>};

determination_from_frame(#stomp_frame{command = subscribe, headers = Headers}) ->
    SubscribeCommand = {subscribe_command, [1_0, 1_1, 1_2], <<"Specified in all versions">>},
    SubscribeAck = subscribe_ack(Headers),
    SubscribeId = subscribe_id(Headers),
    [SubscribeCommand, SubscribeId, SubscribeAck];

determination_from_frame(#stomp_frame{command = ack, headers = Headers}) ->
    AckCommand = {ack_command, [1_0, 1_1, 1_2], <<"Specified in all versions">>},
    ContentLengthFail = case maps:get(<<"id">>, Headers, undefined) of
        undefined -> [];
        <<"content-length-fail">> -> [{content_length, [], <<"Client doesn't support content-length, required in all versions.">>}];
        _ -> []
    end,

    [AckCommand] ++ ContentLengthFail.

subscribe_ack(HeadersMap) ->
    case maps:get(<<"ack">>, HeadersMap, undefined) of
        undefined ->
            {subscribe_ack_header, [1_0, 1_1, 1_2], <<"SUBSCRIBE ack header isn't mandatory, auto is the default in all versions">>};
        <<"auto">> ->
            {subscribe_ack_header, [1_0, 1_1, 1_2], <<"SUBSCRIBE ack header set to auto, supported in all versions">>};
        <<"client">> ->
            {subscribe_ack_header, [1_0, 1_1, 1_2], <<"SUBSCRIBE ack header set to client, supported in all versions">>};
        <<"client-individual">> ->
            {subscribe_ack_header, [1_1, 1_2], <<"SUBSCRIBE ack header set to client-individual, supported from 1.1">>};
        OtherAck ->
            {subscribe_ack_header, [], <<"SUBSCRIBE ack header set to unspecified value: ", OtherAck/binary>>}
    end.

subscribe_id(HeadersMap) ->
    case maps:get(<<"id">>, HeadersMap, undefined) of
        undefined ->
            {subscribe_id_header, [1_0], <<"SUBSCRIBE id header unset, this is only permitted in 1.0">>};
        _ ->
            {subscribe_id_header, [1_0, 1_1, 1_2], <<"SUBSCRIBE id header set, as required from 1.1 and onwards, but optional in 1.0">>}
    end.

host(HeadersMap) ->
    case maps:get(<<"host">>, HeadersMap, undefined) of
        undefined ->
            {host_header, [1_0], <<"host header is undefined, this is only allowed in 1.0">>};
        _HostName ->
            {host_header, [1_0, 1_1, 1_2], <<"host header is set, as required in 1.1 and 1.2 (meaningless but permitted in 1.0)">>}
    end.

accept_version(HeadersMap) ->
    case maps:get(<<"accept-version">>, HeadersMap, undefined) of
        undefined -> 
            {accept_version_header, [1_0], <<"accept-version header is undefined, only allowed in STOMP 1.0">>};
        Header ->
            SplitVersions = binary:split(Header, <<",">>, [global]),
            SanitisedVersions = [version_string_to_int(Version) || Version <- SplitVersions, known_version_string(Version)],
            {accept_version_header, SanitisedVersions, <<"Known versions from accept-version header, which is: ", Header/binary>>}
        end.

known_version_string(Version) ->
    case Version of
        <<"1.0">> -> true;
        <<"1.1">> -> true;
        <<"1.2">> -> true;
        _ -> false
    end.

version_string_to_int(Version) ->
    case Version of
        <<"1.0">> -> 1_0;
        <<"1.1">> -> 1_1;
        <<"1.2">> -> 1_2
    end.

version_int_to_bin(Version) ->
    case Version of
        10 -> <<"1.0">>;
        11 -> <<"1.1">>;
        12 -> <<"1.2">>
    end.