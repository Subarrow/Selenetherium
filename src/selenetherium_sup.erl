%%%-------------------------------------------------------------------
%% @doc selenetherium top level supervisor.
%% @end
%%%-------------------------------------------------------------------

-module(selenetherium_sup).

-behaviour(supervisor).

-export([start_link/0]).

-export([init/1]).

-define(SERVER, ?MODULE).

start_link() ->
    {ok, _Pid} = supervisor:start_link({local, ?SERVER}, ?MODULE, []).

init([]) ->
    SupFlags = #{strategy => one_for_all,
                 intensity => 0,
                 period => 1},
    ChildSpecs = [
        #{id => selenetherium_server, start => {selenetherium, server_link, []}}
    ],
    {ok, {SupFlags, ChildSpecs}}.

%% internal functions
